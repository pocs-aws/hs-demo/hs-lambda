'use strict';

let AWS = require('aws-sdk');

const assumeRole = async () => {
    const sts = new AWS.STS({apiVersion: '2011-06-15'});
    const roleToAssume = process.env.ROLE_GET_SECRET;

    console.log('entre al assumeRole 2', process.env.ROLE_GET_SECRET);
    const data = await sts.assumeRole({
        RoleArn: roleToAssume,
        RoleSessionName: 'SessionExample'
    }).promise();

    if (!data.Credentials) {
        throw new Error('Invalid credentials');
    }
    const roleCredentials = {
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    };
    console.log('ASSUMED ROLE: TOKEN GENERATED:', roleToAssume);
    return roleCredentials;
};

const getSecrets = async (roleCredentials) => {
    const SecretsManager = AWS.SecretsManager;
    const secretId = process.env.SECRET_ARN;
    let data = null;
    const secretsManager = new SecretsManager({
        region: process.env.REGION,
        ...roleCredentials
    });
    console.log('roleCredentials getSecrets', roleCredentials);
    console.log('secretId', secretId);
    const describeSecret = await secretsManager
        .describeSecret({
            SecretId: secretId
        })
        .promise();
    console.log('describeSecret', describeSecret);
    const secretValue = await secretsManager
        .getSecretValue({
            SecretId: secretId
        })
        .promise();
    console.log('secret value', secretValue);
    console.log('secret SecretString value', secretValue.SecretString);
    if ('SecretString' in secretValue) {
        data = secretValue.SecretString;
    } else {
        let buff = new Buffer(secretValue.SecretBinary, 'base64');
        data = buff.toString('ascii');
    }
    return JSON.parse(data);
};

const generate_joke = async (apiJokesKey) => {
    const axios = require('axios');
    const options = {
        method: 'GET',
        url: `${process.env.API_JOKES_ENDPOINT}/generate-joke`,
        headers: {
            'X-RapidAPI-Key': apiJokesKey,
            'X-RapidAPI-Host': process.env.API_JOKES_HEADER_HOST
        }
    };
    const res = await axios.request(options);
    return res.data;
};


module.exports.handler = async (data, ctx, cb) => {
    try {
        console.log(data);
        const roleCredentials = await assumeRole();
        const secrets = await getSecrets(roleCredentials);
        console.log('secrets 63', secrets);
        const joke = await generate_joke(secrets.apis.apiJokesKey);
        console.log('JOKE', joke)
    } catch (err) {
        console.error(err.message);
        throw err;
    }
};
